# SpotifyCollector

This tool enables a Spotify-user to automatically create playlists.


## Setup

You need to create an app in your spotify account and import the CLIENT-ID as well as the SECRET.
More details: https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app


Similar to your google account, which needs to provide an API-KEY with access to the Youtube-API.
More details: https://console.developers.google.com/

## Install

```bash
sudo apt install encfs
virtualenv -p -python3 .venv_spotify_collector
# Create a virtual environment or use your regular python3-environment.
source .venv_spotify_collector/bin/activate
pip3 install -r requirements.txt
# Install requirements
./installer.sh
```

Choose if you want to encrypt your local keys. 
This will make the tool to ask for the decryption passwords during every execution.

## Run

```bash
python3 run_callback.py &> /dev/null &
./executor.sh "<query>" "<username>"
# Callback Server will still run after execution. Ctrl+C to kill it or close the Terminal
```

