#!/usr/bin/python
import sys, os, json, time, subprocess as sb, logging
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser


config = json.load(open(".config.json","r"))

def run_youtube_query(verbosity=0, *args):
	if verbosity <= 0:
		logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
	if verbosity == 1:
		logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
	else:
		logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
	def load_key():
		global YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, DEVELOPER_KEY
		root_path = os.path.join(os.getcwd(), ".keys")
		YOUTUBE_API_SERVICE_NAME = "youtube"
		YOUTUBE_API_VERSION = "v3"
		if config["ENCRYPT"]:
			logging.info("Decrypting Google-API-keys...")
			sb.Popen(["encfs", os.path.join(root_path,".infos_encfs_google"),
				os.path.join(root_path,"infos")]).communicate()
			key_file = os.path.join(root_path,"infos/api_keys.txt")
		else:
			logging.info("Loading Google-API-keys...")
			key_file = os.path.join(root_path,"infos/google_api_keys.txt")
		with open(key_file,"r") as f_in:
			DEVELOPER_KEY=f_in.readline().split("=")[-1].strip()
		if config["ENCRYPT"]:
			time.sleep(1)
			sb.Popen(["fusermount","-u",os.path.join(root_path,"infos")]).communicate()
			logging.info("Finished using Google-API-keys. Encrypted storage again.")

	def youtube_search(query, max_results):
		youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
			developerKey=DEVELOPER_KEY)
		logging.debug("Created Youtube-API-interface.")

		search_response = youtube.search().list(
			q=query,
			part="id,snippet",
			maxResults=max_results,
			type="video"
		).execute().get("items",[])
		logging.debug('Obtained %d results from query: "%s".' % (len(search_response), str(query)))

		video_ids = ",".join(map(lambda v:v["id"]["videoId"], search_response))
		videos = youtube.videos().list(
				part="id,snippet",
				id=video_ids,
				).execute()
		videos = [{"title":v["snippet"]["title"], "description":v["snippet"]["description"]}
				for v in videos["items"]]
		logging.info('Obtained %d videos from query: "%s".' % (len(videos), str(query)))

		fp = os.path.join(config["DATAPATH"], "query_result_"+query.replace(" ","_")+".json")
		json.dump(videos, open(fp, "w"), indent=2)
		logging.debug('Wrote results to: %s.' % fp)


	try:
		load_key()
		youtube_search(*args)
	except HttpError as e:
		logging.error("An HTTP error %d occurred:\n%s" % (e.resp.status, e.content))

if __name__ == "__main__":
	argparser.add_argument("-q", help="Search term", default="Google", dest="query")
	argparser.add_argument("--max-results", help="Max results", default=25, dest="max_results")
	argparser.add_argument("-v", help="Verbosity", action="count", default=0, dest="verbosity")
	args = argparser.parse_args()
	run_youtube_query(args.verbosity, args.query, args.max_results)
