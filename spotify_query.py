import sys, os, json, time, subprocess as sb, logging
from tqdm import tqdm
from argparse import ArgumentParser
import spotipy
from spotipy.client  import SpotifyException
import spotipy.util as util

config = json.load(open(".config.json","r"))

def run_spotify_playlist_creation(verbosity=0, *args):
	if verbosity <= 0:
		logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
	if verbosity == 1:
		logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
	else:
		logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
	def load_key():
		global SPOTIPY_CLIENT_ID, SPOTIPY_CLIENT_SECRET, SPOTIPY_REDIRECT_URL
		root_path = os.path.join(os.getcwd(), ".keys")
		if config["ENCRYPT"]:
			logging.info("Decrypting Spotify-API-keys...")
			sb.Popen(["encfs", os.path.join(root_path,".infos_encfs_spotify"),
				os.path.join(root_path,"infos")]).communicate()
			key_file = os.path.join(root_path,"infos/api_keys.txt")
		else:
			logging.info("Loading Spotify-API-keys...")
			key_file = os.path.join(root_path,"infos/spotify_api_keys.txt")

		with open(key_file,"r") as f_in:
			SPOTIPY_CLIENT_ID=f_in.readline().split("=")[-1].strip()
			SPOTIPY_CLIENT_SECRET=f_in.readline().split("=")[-1].strip()
		SPOTIPY_REDIRECT_URL='http://localhost:5454/'

		#Delay for unmount encfs-directory
		if config["ENCRYPT"]:
			time.sleep(1)
			sb.Popen(["fusermount","-u",os.path.join(root_path,"infos")]).communicate()
			logging.info("Finished using Spotify-API-keys. Encrypted storage again.")


	def chunks(l, n):
		n = max(1, n)
		return (l[i:i+n] for i in range(0, len(l), n))

	def search_for_track(track, sp):
		return sp.search(q=track, type="track", limit=1)

	def spotify_search(username, playlist_name, fin, fout):
		scope = 'playlist-modify-private playlist-modify-public'
		token = util.prompt_for_user_token(username,scope,client_id=SPOTIPY_CLIENT_ID,
				client_secret=SPOTIPY_CLIENT_SECRET ,redirect_uri=SPOTIPY_REDIRECT_URL)
		tracks_formatted_description = json.load(open(fin,"r"))
		logging.info('Loaded queries from file: "%s"' % str(fin))

		if token:
			logging.info("Obtained Token for Spotify-Requests")
			sp = spotipy.Spotify(auth=token)
			current_user = sp.current_user()
			logging.info("Current User:\n%s" % str(current_user))
			created_playlist = sp.user_playlist_create(current_user["id"], playlist_name, public=False)
			logging.info("Created Playlist:\n%s" % str(playlist_name))
			try:
				track_data = json.load(open(fout,"r"))
				logging.debug('Loaded existing data to the query from: "%s"' % str(fout))
			except Exception as e:
				track_data = {}
				logging.debug('No existing data found for the query at: "%s"' % str(fout))
			search_queries = [track for track in tracks_formatted_description
					if track not in track_data]
			logging.info("Amount of queries: %d" % len(search_queries))
			for track in tqdm(search_queries):
				try:
					track_data.update({track:search_for_track(track, sp)})
				except Exception as e:
					logging.debug('Exception occurred for query: "%s"\n%s\n%s\n%s'
							%(str(track), "_"*80, str(e), "_"*80))
					continue
			json.dump(track_data, open(fout,"w"), indent=2)
			logging.info('Saved retrieved tracks in: "%s"' % str(fout))
			track_ids = list(
					map(lambda t: t["tracks"]["items"][0]["id"],
				filter(lambda t: t["tracks"]["items"] != [], track_data.values()) ) )
			logging.info("Amount of found tracks: %d" % len(track_ids))
			for chunk in chunks(track_ids, 100):
				ans = sp.user_playlist_add_tracks(username, created_playlist["id"], chunk)
				logging.debug('Repsonse for adding tracks to playlist:\n%s' % str(ans))
		else:
			logging.error("No Token obtained for Spotify-Requests")

	load_key()
	spotify_search(*args)

if __name__ == "__main__":
	parser = ArgumentParser()
	parser.add_argument("-u", "--username", help="username of Spotify Account",
			default="", dest="username")
	parser.add_argument("-l", "--playlistname", help="Name of new Playlist",
			default="Some_Playlist", dest="playlist_name")
	parser.add_argument("-f", "--inputfile", help="input file from formatted youtube-query",
			default=os.path.join(config["DATAPATH"], "tracks_formatted_description.json"), dest="fin")
	parser.add_argument("-o", "--outputfile", help="output file",
			default=os.path.join(config["DATAPATH"], "tracks_result.json"), dest="fout")
	parser.add_argument("-v", help="Verbosity", action="count", default=0, dest="verbosity")
	args = parser.parse_args()
	run_spotify_playlist_creation(args.verbosity, args.username, args.playlist_name, args.fin, args.fout)

