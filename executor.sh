#!/bin/bash
QUERY=$1
USERNAME=$2
echo "Using Query: $QUERY"
QUERY_FORMATTED=$(echo $QUERY | sed 's/\ /_/g')
DATAPATH=$(pwd)/$(python3 -c 'import json; config = json.load(open(".config.json","r"));config["DATAPATH"]=".data";print(config["DATAPATH"])')
QUERY_RESULT=$DATAPATH/query_result_$QUERY_FORMATTED.json
FORMATTED_QUERY_RESULT=$DATAPATH/query_result_formatted_$QUERY_FORMATTED.json
TRACKS_RESULT=$DATAPATH/tracks_result_$QUERY_FORMATTED.json
python3 youtube_query.py -q "$QUERY" --max-results 10
python3 formatter.py -f $QUERY_RESULT -o $FORMATTED_QUERY_RESULT
python3 spotify_query.py -u "$USERNAME" -f $FORMATTED_QUERY_RESULT -o $TRACKS_RESULT -l $QUERY_FORMATTED -v

