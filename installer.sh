#!/bin/bash

### Tries to locate API-KEYS directory ###
KEY_DIRECTORY=$(pwd)/.keys
if [ ! -d $KEY_DIRECTORY ]; then
	mkdir -v ./.keys	
	mkdir -v ./.keys/infos

fi

if [ ! -d $KEY_DIRECTORY/keys/infos ]; then
	mkdir -v ./.keys/infos
fi

### Enable Encryption for the API-KEYS ###
read -p "The next step requires your API-KEYS for Spotify and Google. Do you want to encrypt them in this project(y|n)?" ans
case "$ans" in 
	y|Y ) ENCRYPT=true; echo "Going to encrypt API-Keys...";;
	* ) ENCRYPT=false; echo "Going to leave API-Keys unencrypted...";;
esac

### Export Encryption settings ###
if $ENCRYPT; then
	python3 -c 'import json; config = json.load(open(".config.json","r"));config["ENCRYPT"]=True;json.dump(config, open(".config.json","w"),indent=2)'
	encfs $KEY_DIRECTORY/.infos_encfs_spotify $KEY_DIRECTORY/infos
	SPOTIFY_API_FILE=$KEY_DIRECTORY/infos/api_keys.txt
else
	python3 -c 'import json; config = json.load(open(".config.json","r"));config["ENCRYPT"]=False;json.dump(config, open(".config.json","w"),indent=2)'
	SPOTIFY_API_FILE=$KEY_DIRECTORY/infos/spotify_api_keys.txt
fi

read -p "Please enter your Spotify-Client-ID: " SPOTIFY_CLIENT_ID
read -p "Please enter your Spotify-Client-SECRET: " SPOTIFY_CLIENT_SECRET
echo "SPOTIFY_CLIENT_ID="$SPOTIFY_CLIENT_ID > $SPOTIFY_API_FILE
echo "SPOTIFY_CLIENT_SECRET="$SPOTIFY_CLIENT_SECRET >> $SPOTIFY_API_FILE

if $ENCRYPT; then
	sleep 1
	fusermount -u $KEY_DIRECTORY/infos
	encfs $KEY_DIRECTORY/.infos_encfs_spotify $KEY_DIRECTORY/infos
	GOOGLE_API_FILE=$KEY_DIRECTORY/infos/api_keys.txt
else
	GOOGLE_API_FILE=$KEY_DIRECTORY/infos/google_api_keys.txt
fi

read -p "Please enter your Google-API-Keys: " GOOGLEAPI_KEY
echo "GOOGLEAPI_KEY="$GOOGLEAPI_KEY > $GOOGLE_API_FILE
if $ENCRYPT; then
	sleep 1
	fusermount -u $KEY_DIRECTORY/infos
fi


DATAPATH=$(pwd)/$(python3 -c 'import json; config = json.load(open(".config.json","r"));config["DATAPATH"]=".data";print(config["DATAPATH"])')
if [ ! -d $DATAPATH ]; then
	mkdir $DATAPATH
fi
