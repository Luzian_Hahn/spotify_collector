import json, os, re, string, unicodedata, logging
from argparse import ArgumentParser

config = json.load(open(".config.json","r"))
def format_youtube_response(fin, fout, verbosity=0):
	if verbosity <= 0:
		logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
	if verbosity == 1:
		logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
	else:
		logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

	data = json.load(open(fin,"r"))
	logging.info('Successfully loaded data from: "%s"' % str(fin))

	special_chars = re.escape(string.punctuation)
	description_lines = "".join(map(lambda v: v["description"], data)).split("\n")
	detection_patterns = [
			lambda d: re.match(".*[0-9]+:[0-9]{1}.*",d) != None,
			lambda d: re.match("[0-9]+[).]?.*",d)
			]
	### Only Tracklistitems with hooks
	description_lines = filter(lambda d: any(map(lambda pattern: pattern(d), detection_patterns)) ,description_lines)
	logging.debug("Considering only Trackitems with hooks")
	### Remove hooks
	description_lines = map(lambda d: re.sub("[0-9]+:[0-9]{1}","",d), description_lines)
	logging.debug("Removed hooks")
	### Remove numbers
	description_lines = map(lambda d: re.sub("[0-9]+","",d), description_lines)
	logging.debug("Removed numbers")
	### Remove any specialchars. Replace uppercase letters by lowercase
	description_lines = map(lambda d: re.sub(r'['+special_chars+']', '', d.lower()), description_lines )
	logging.debug("Removed any specialchars. Replaced uppercase letters by lowercase")
	### Remove accents from letters
	description_lines = map(lambda d: unicodedata.normalize("NFD",d).encode("ascii","ignore").decode("utf-8").strip()  ,description_lines)
	logging.debug("Removed accents from letters")
	### Replace multiple spaces
	description_lines = map(lambda d: re.sub('[ ]+', " ",d), description_lines)
	logging.debug("Replaced multiple spaces")

	json.dump(list(description_lines), open(fout, "w"),indent=2)
	logging.info('Saved formatted results to: "%s"' % str(fout))

if __name__ == "__main__":
	parser = ArgumentParser()
	parser.add_argument("-f", "--inputfile", help="input file from youtube-query",
			default=os.path.join(config["DATAPATH"], "tmp_result.json"), dest="fin")
	parser.add_argument("-o", "--outputfile", help="output file", default=os.path.join(config["DATAPATH"],
		"tracks_formatted_description.json"), dest="fout")
	parser.add_argument("-v", help="Verbosity", action="count", default=0, dest="verbosity")
	args = parser.parse_args()
	format_youtube_response(args.fin, args.fout, args.verbosity)
